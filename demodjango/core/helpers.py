from django.http import HttpResponse
import requests
import json


def connect_api(url, method='get', data='', key=''):
    try:
        header = {
            "Content-Type": "application/json",
            "Accept": "application/json",
        }
        params = {
            'keyEncrypt': 'jfwVrPPbV6ZNKKPpk8R5',
            key: data
        }

        if method == 'get':
            result = requests.get(url, params=params, headers=header).json()['datas']
            return result
        else:
            result = requests.post(url, json=params, headers=header)

        if result.status_code == 200:
            return result.json()['datas']
    except:
        return HttpResponse("Something went wrong!")


def connect_api_by_multi_params(url, method='get', data=(), key=()):
    try:
        header = {
            "Content-Type": "application/json",
            "Accept": "application/json",
        }
        params = {
            'keyEncrypt': 'jfwVrPPbV6ZNKKPpk8R5',
            key[0]: data[0],
            key[1]: int(data[1])
        }

        result = requests.post(url, json=params, headers=header)

        if result.status_code == 200:
            return result.json()['datas']
    except:
        return HttpResponse("Something went wrong!")