from .helpers import connect_api


def get_lasted_list_blog():
    url = 'https://azoffice.vn/api/v8/azoffice/getListPostLastest'
    method = "post"
    data = ""
    return connect_api(url, data, method)


def get_list_posts_activities_azoffice():
    url = 'https://azoffice.vn/api/v8/azoffice/getListPost'
    method = "post"
    data = ""
    return connect_api(url, data, method)['hoat-dong-azoffice']['data']


def get_list_posts_office_decoration():
    url = 'https://azoffice.vn/api/v8/azoffice/getListPost'
    method = "post"
    data = ""
    return connect_api(url, data, method)['trang-tri-van-phong']['data']


def get_list_posts_generalknowledge():
    url = 'https://azoffice.vn/api/v8/azoffice/getListPost'
    method = "post"
    data = ""
    return connect_api(url, data, method)['kien-thuc-chung']['data']


def get_blog_building_page():
    url = 'https://azoffice.vn/api/v8/azoffice/getPostInBuildingPage'
    method = "post"
    data = ""
    return connect_api(url,data, method)


def get_blog_by_category(data, page=0):
    url = 'https://azoffice.vn/api/v8/azoffice/getPostByCategory'
    connect_api(url)

def get_detail_blog(data='', page=0):
    url = 'https://azoffice.vn/api/v8/azoffice/getDetailPost'
    method = "post"
    key = "slugPost"
    return connect_api(url, method, data, key)


