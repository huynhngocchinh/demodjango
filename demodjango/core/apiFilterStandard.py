from .helpers import connect_api


def get_api_list_filter_standard():
    url = 'https://azoffice.vn/api/v3/azoffice/getListRank'
    method = "post"
    return connect_api(url, method)


def get_api_filter_building_feature_by_standard(data, page):
    url = f'https://azoffice.vn/api/v3/azoffice/getBuildingByRank?page={page}'
    method = "post"
    return connect_api(url, method, data)
