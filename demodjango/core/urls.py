from django.urls import path
from . import views


urlpatterns = [
    path('', views.homepage, name='homepage'),
    path('van-phong/van-phong-truyen-thong', views.traditional_building, name='building_list'),
    path('van-phong/chi-tiet-van-phong/<str:slug>', views.detail_building, name='building_detail'),
    path('van-phong/van-phong-theo-quan/<str:slug>', views.list_building_in_district, name='building_in_district'),
    path('van-phong/<str:type_filter>/<str:type_price>/<str:id_type>/<int:page>', views.list_building_filter_in_price, name='building_filter'),
    path('blog', views.blog, name='blog_list'),
    path('blog/<str:slug>', views.blogDetails, name='chi-tiet-tin-tuc'),

]