from .helpers import connect_api


def get_list_type_building(request):
    url = 'https://azoffice.vn/api/v6/azoffice/getListTypeBuilding'
    return connect_api(url)


def get_district_for_menu(request):
    url = 'https://azoffice.vn/api/v7/azoffice/getListDistrictInHeader'
    return connect_api(url)


def get_api_detail_building(data):
    url = 'https://azoffice.vn/api/v1/azoffice/getDetailBuilding'
    method = "post"
    key = "urlBuiding"
    return connect_api(url, method, data, key)


def get_api_list_building_feature(building_type, page):
    url = f'https://azoffice.vn/api/v1/azoffice/getListBuildingFeature?page={page}'
    method = "get"
    return connect_api(url, method, building_type)


def get_count_office_by_district_in_header():
    url = 'https://azoffice.vn/api/v7/azoffice/getListDistrictInHeader'
    method = "post"
    return connect_api(url, method)


def get_count_office_by_district(data):
    url = 'https://azoffice.vn/api/v7/azoffice/getDistrictByProvince'
    method = "post"
    key = "typeSlug"
    return connect_api(url, method, data, key)


def get_list_traditional_feature_building(data, page):
    url = f'https://azoffice.vn/api/v1/azoffice/getBuildingByType?page={page}'
    method = "post",
    key = "typeSlug"
    return connect_api(url, method, data, key)


# def get_building_by_type(data, page):


# def v_members(request):
#     # myMembers = Member.objects.all().values()
#
#     template = loader.get_template('homepage.html')
#     context = {
#         'myMembers': myMembers,
#     }
#     return HttpResponse(template.render(context, request))
#
#
# def members(request):
#     return HttpResponse("Hello world!")


