from .helpers import connect_api, connect_api_by_multi_params


def get_api_list_filter_road(data):
    url = 'https://azoffice.vn/api/v7/azoffice/getListProvince'
    method = "post"
    return connect_api(url, method, data)


def get_api_filter_building_feature_by_road(data, page):
    url = f'https://azoffice.vn/api/v3/azoffice/getBuildingByRoad?page={page}'
    method = "post"
    return connect_api(url, method, data)


def get_api_building_in_the_same_road(data):
    url = f'https://azoffice.vn/api/v1/azoffice/getBuildingByStreet'
    method = "post"
    key = ("urlBuilding", "streetBuilding")
    return connect_api_by_multi_params(url, method, data, key)


