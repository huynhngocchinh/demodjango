from .helpers import connect_api, connect_api_by_multi_params


def get_list_district_in_province():
    url = 'https://azoffice.vn/api/v7/azoffice/getDistrictByProvinceId'
    method = "post"
    return connect_api(url, method)


def get_api_list_filter_district(data, page):
    url = f'https://azoffice.vn/api/v1/azoffice/getBuildingsInDistrict?page={page}'
    method = "post"
    key = ("typeBuilding", "districtBuilding")
    return connect_api_by_multi_params(url, method, data, key)


def get_api_district_by_id(data):
    url = f'https://azoffice.vn/api/v7/azoffice/getDistrictById'
    method = "post"
    return connect_api(url, method, data)


def get_api_building_in_the_same_district(data):
    url = f'https://azoffice.vn/api/v1/azoffice/getBuildingByDistrict'
    method = "post"
    key = ("urlBuilding", "districtBuilding")
    return connect_api_by_multi_params(url, method, data, key)
