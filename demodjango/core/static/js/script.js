function openNav() {
    document.getElementById("mySidebar").style.width = "220px";
    document.getElementById("main").style.marginRight = "220px";
}

function closeNav() {
    document.getElementById("mySidebar").style.width = "0";
    document.getElementById("main").style.marginRight= "0";
}
$(document).ready(function () {
    $("#tag-knowledge").click(function(){
        $("#content-knowledge").toggle();
    });
    $("#tag-activity").click(function(){
        $("#content-activity").toggle();
    });
	$(window).scroll(function () {
	  if ($(this).scrollTop() > 100) {
		$('.scroll-top').fadeIn();
	  } else {
		$('.scroll-top').fadeOut();
	  }
	});

	$('.scroll-top').click(function () {
	  $("html, body").animate({
		scrollTop: 0
	  }, 100);
		return false;
	});

  });

// compare office
$(document).ready(function() {
    var arrListUrlOfficeCompareShow = JSON.parse(localStorage.getItem("hiddenUrlOffice"));
    if(arrListUrlOfficeCompareShow) {
        $('#nb_office_compare').addClass('lbl-save-office');
        $('#nb_office_compare').text(arrListUrlOfficeCompareShow.length);
        arrListUrlOfficeCompareShow.forEach((entry) => {
            $('#'+entry.url).css('color', 'red');
        });
    }
    $(".btn-close-popup").click(function(){
        $("#popup-save-office").hide();
        $("#popup-seen-save-office").hide();
        $(".popup-not-result").hide();
      });
});
function deleteListCompareOffice(urlBuilding){
    var arrListUrlOfficeCompareShow = JSON.parse(localStorage.getItem("hiddenUrlOffice"));
    arrListUrlOfficeCompareShow.forEach((entry) => {
        if(urlBuilding.trim() == entry.url) {
            const index = arrListUrlOfficeCompareShow.indexOf(entry);
            arrListUrlOfficeCompareShow.splice(index, 1);
        }
    });
    localStorage.setItem("hiddenUrlOffice", JSON.stringify(arrListUrlOfficeCompareShow));
    var arrListUrlOfficeCompareShow = JSON.parse(localStorage.getItem("hiddenUrlOffice"));
    if(arrListUrlOfficeCompareShow.length < 1) {
        $('#nb_office_compare').addClass('lbl-save-office');
        $('#nb_office_compare').text('');
    } else {
        $('#nb_office_compare').text(arrListUrlOfficeCompareShow.length);
    }

    var routeCompare = "/so-sanh-van-phong";
    var arrListUrlOfficeCompareShow2 = JSON.parse(localStorage.getItem("hiddenUrlOffice"));
    if(arrListUrlOfficeCompareShow2) {
        var dataPost = '';
        arrListUrlOfficeCompareShow2.forEach((entry) => {
            dataPost = (dataPost) ? dataPost+ ';;' +entry.url : entry.url;
        });
        window.location.href = routeCompare+'?data_compare='+dataPost;
    } else {
        window.location.href = routeCompare;
    }
}
function deleteListSaveOffice(a){
    var data = {
        urlOffice: a
    };
    $('.loader').show();
    $.ajax({
        url: '/xoa-toa-nha',
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
        type: 'POST',
        data: data,
        success: function(response) {
            $('.loader').hide();
            if (response.error == 0) {
                alert('thanh cong');
            }
            location.reload();
        },
        error: function(response) {
            alert('that bai');
        }
    });
}
// var numberSeenOffice = Number(document.getElementById('numberCompareOffice').value);
// compare office

function addToListSeenOffice(a) {
    var numberSeenOffice = document.getElementById('oldAppointment').value;
    if(parseInt(numberSeenOffice) == 4){
        alert('Chỉ được chọn tối đa 4 tòa nhà ');
    }else{
        var data = {
            urlOffice: a
        };
        $('.loader').show();
        $.ajax({
            url: '/luu-toa-nha',
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            type: 'POST',
            data: data,
            success: function(response) {
                $('.loader').hide();
                if (response.error == 0) {
                    alert('thanh cong');
                }
            var numberSeenOffice = document.getElementById('oldAppointment').value;
            if(numberSeenOffice === undefined){
                numberSeenOffice = 0;
            }
            document.getElementById('oldAppointment').value = parseInt(numberSeenOffice) + 1
                $('.lbl-save-office-for-seen').text(parseInt(numberSeenOffice) + 1);
                setTimeout(function(){ $("#popup-save-office").show(); }, 2000);
            },
            error: function(response) {
                alert('Tòa nhà đã tồn tại trong danh sách hẹn xem!');
            }
        });
    }
}

function removeElementItemCompare(urlBuilding) {
    $('#tr_'+urlBuilding).remove();
    var arrListUrlOfficeCompareShow = JSON.parse(localStorage.getItem("hiddenUrlOffice"));
    arrListUrlOfficeCompareShow.forEach((entry) => {
        if(urlBuilding.trim() == entry.url) {
            const index = arrListUrlOfficeCompareShow.indexOf(entry);
            arrListUrlOfficeCompareShow.splice(index, 1);
        }
    });
    localStorage.setItem("hiddenUrlOffice", JSON.stringify(arrListUrlOfficeCompareShow));
}

function addToListCompareOffice(urlOffice) {
    var compareOffice = document.getElementById(urlOffice);
    var listInforOfficeCompare = [];
    var dataSaveNew = {
        name : compareOffice.dataset.name,
        image : compareOffice.dataset.image,
        price : compareOffice.dataset.price,
        square : compareOffice.dataset.square,
        address : compareOffice.dataset.address,
        url : compareOffice.dataset.url
    };
    var linkUrlOfficeOld = localStorage.getItem("hiddenUrlOffice");
    var arrListUrlOfficeCompare = JSON.parse(linkUrlOfficeOld);

    if (linkUrlOfficeOld && arrListUrlOfficeCompare.length > 0) {
        if (arrListUrlOfficeCompare.length >= 4) {
            setTimeout(function(){ alert("Chỉ tối đa 4 tòa nhà so sánh cùng lúc!"); }, 2000);
            return false;
        }
        var checkExist = false;
        arrListUrlOfficeCompare.forEach((entry) => {
            if (entry.url == compareOffice.dataset.url) {
                checkExist = true;
            }
        });
        if(checkExist) {
            setTimeout(function(){ alert("Tòa nhà đã tồn tại trong danh sách so sánh!"); }, 2000);
            return false;
        }
        arrListUrlOfficeCompare.push(dataSaveNew);
        localStorage.setItem("hiddenUrlOffice", JSON.stringify(arrListUrlOfficeCompare));
        var htmlListBuildingCompare = '';
        arrListUrlOfficeCompare.forEach((entry) => {
            htmlListBuildingCompare += '<tr class="office-save-item" id="tr_'+entry.url+'">'+
            '<td scope="row">'+
                '<img class="image-appointment" src="'+entry.image+'" alt="'+entry.name+'">'+
            '</td>'+
            '<td>'+
                '<h2 class="name-saving-item" id="nameBuildingModal">'+entry.name+'</h2>'+
                '<p class="location-saving-item" id="addressBuildingModal"><i class="fa fa-map-marker "></i>'+entry.address+'</p>'+
                '<p class="price-saving-item"><i class="fa fa-square"></i> Giá thuê (m2): <span id="priceBuildingModal">'+entry.price+' USD/m2</span></p>'+
            '</td>'+
            '<td><div class="btn-delete-saving-item xemthem"><a href="javascript:void(0)" onClick="removeElementItemCompare('+"'"+entry.url+"'"+')"><i class="fa fa-trash"></i> </a></td>'+
        '</tr>';
        });
        var htmlListBuildingCompare =
        $('#tbody_listCompareOffice').html(htmlListBuildingCompare);
        $('#popup-seen-save-office').show();
        // setTimeout(function() {
        //     $('#popup-seen-save-office').hide();
        // }, 5000);
    } else {
        listInforOfficeCompare.push(dataSaveNew);
        localStorage.setItem("hiddenUrlOffice", JSON.stringify(listInforOfficeCompare))
        setTimeout(function(){ alert("Đã Lưu Vào Danh Sách So Sánh"); }, 2000);
    }
    $('#'+urlOffice).css('color', 'red');
    var arrListUrlOfficeCompareShow = JSON.parse(localStorage.getItem("hiddenUrlOffice"));
    $('#nb_office_compare').addClass('lbl-save-office');
    $('#nb_office_compare').text(arrListUrlOfficeCompareShow.length);
}

// owl carousel
$(document).ready(function(){
    $("#locationTab").click(function (){
        $('.tablinks2').removeClass('active');
        $(this).addClass('active');
        $('html, body').animate({
            scrollTop: $("#contentLocation").offset().top - 100
        }, 2000);
    });
    $("#introTab").click(function (){
        $('.tablinks2').removeClass('active');
        $(this).addClass('active');
        $('html, body').animate({
            scrollTop: $("#contentIntro").offset().top - 100
        }, 2000);
    });
    $("#structTab").click(function (){
        $('.tablinks2').removeClass('active');
        $(this).addClass('active');
        $('html, body').animate({
            scrollTop: $("#contentStruct").offset().top - 100
        }, 2000);
    });
    $("#questionTab").click(function (){
        $('.tablinks2').removeClass('active');
        $(this).addClass('active');
        $('html, body').animate({
            scrollTop: $("#contentQuestion").offset().top - 100
        }, 2000);
    });
    var owlOutstanding = $('.owl-traditional-office-outstanding');
    if (owlOutstanding.length) {
        owlOutstanding.owlCarousel({
            items: 7,
            rewind: true,
            autoplay: true,
            loop: false,
            lazyLoad: false,
            mouseDrag: true,
            touchDrag: true,
            margin: 10,
            smartSpeed: 250,
            autoplaySpeed: 1000,
            nav: false,
            dots: false,
            responsiveClass:true,
            responsiveRefreshRate: 200,
            responsive: {
                0: {
                    items: 2,
                    margin: 0
                },
                400: {
                    items: 3,
                    margin: 10
                },
                600: {
                    items: 4,
                    margin: 10
                },
                768: {
                    items:5,
                    margin: 10
                },
                992: {
                    items: 6,
                    margin: 20
                },
                1200: {
                    items: 7,
                    margin: 10
                }
            }
        });
    }
    var owlNews = $('.owl-tintuc');
    if (owlNews.length) {
        owlNews.owlCarousel({
            items: 7,
            rewind: true,
            autoplay: true,
            loop: false,
            lazyLoad: false,
            mouseDrag: true,
            touchDrag: true,
            margin: 10,
            smartSpeed: 250,
            autoplaySpeed: 1000,
            nav: false,
            dots: false,
            responsiveClass:true,
            responsiveRefreshRate: 200,
            responsive: {
                0: {
                    items: 1,
                    margin: 0
                },
                400: {
                    items: 2,
                    margin: 10
                },
                600: {
                    items: 2,
                    margin: 10
                },
                768: {
                    items:2,
                    margin: 10
                },
                992: {
                    items: 3,
                    margin: 15
                },
                1200: {
                    items: 3,
                    margin: 20
                }
            }
        });
    }
    // internal operation
    var owlInternalOperation = $('.owl-internal-operation');
    if (owlInternalOperation.length) {
        owlInternalOperation.owlCarousel({
            items: 7,
            rewind: true,
            autoplay: true,
            loop: false,
            lazyLoad: false,
            mouseDrag: true,
            touchDrag: true,
            margin: 10,
            smartSpeed: 250,
            autoplaySpeed: 1000,
            nav: false,
            dots: true,
            responsiveClass:true,
            responsiveRefreshRate: 200,
            responsive: {
                0: {
                    items: 1,
                    margin: 0
                },
                400: {
                    items: 1,
                    margin: 10
                },
                600: {
                    items: 2,
                    margin: 10
                },
                768: {
                    items:2,
                    margin: 10
                },
                992: {
                    items: 3,
                    margin: 20
                },
                1200: {
                    items: 3,
                    margin: 20
                }
            }
        });
    }

    // internal operation
    var owlInternalOperation3 = $('.owl-internal-operation-3');
    if (owlInternalOperation3.length) {
        owlInternalOperation3.owlCarousel({
            items: 7,
            rewind: true,
            autoplay: true,
            loop: false,
            lazyLoad: false,
            mouseDrag: true,
            touchDrag: true,
            margin: 10,
            smartSpeed: 250,
            autoplaySpeed: 1000,
            nav: false,
            dots: true,
            responsiveClass:true,
            responsiveRefreshRate: 200,
            responsive: {
                0: {
                    items: 1,
                    margin: 0
                },
                400: {
                    items: 1,
                    margin: 10
                },
                600: {
                    items: 1,
                    margin: 10
                },
                768: {
                    items:1,
                    margin: 10
                },
                992: {
                    items: 2,
                    margin: 20
                },
                1200: {
                    items: 2,
                    margin: 20
                }
            }
        });
    }


    // internal operation
    var owlGeneralIntro = $('.owl-general-intro');
    if (owlGeneralIntro.length) {
        owlGeneralIntro.owlCarousel({
            items: 7,
            rewind: true,
            autoplay: true,
            loop: false,
            lazyLoad: false,
            mouseDrag: true,
            touchDrag: true,
            margin: 10,
            smartSpeed: 250,
            autoplaySpeed: 1000,
            nav: false,
            dots: true,
            responsiveClass:true,
            responsiveRefreshRate: 200,
            responsive: {
                0: {
                    items: 1,
                    margin: 20
                },
                400: {
                    items: 1,
                    margin: 10
                },
                600: {
                    items: 2,
                    margin: 15
                },
                768: {
                    items:2,
                    margin: 20
                },
                992: {
                    items: 3,
                    margin: 20
                },
                1200: {
                    items: 3,
                    margin: 20
                }
            }
        });
    }

    // internal operation
    var owlDetailOffice = $('.owl-detail-office');
    if (owlDetailOffice.length) {
        owlDetailOffice.owlCarousel({
            items: 7,
            rewind: true,
            autoplay: true,
            loop: false,
            lazyLoad: false,
            mouseDrag: true,
            touchDrag: true,
            margin: 10,
            smartSpeed: 250,
            autoplaySpeed: 1000,
            nav: false,
            dots: true,
            responsiveClass:true,
            responsiveRefreshRate: 200,
            responsive: {
                0: {
                    items: 1,
                    margin: 20
                },
                400: {
                    items: 1,
                    margin: 10
                },
                600: {
                    items: 2,
                    margin: 15
                },
                768: {
                    items:2,
                    margin: 20
                },
                992: {
                    items: 3,
                    margin: 20
                },
                1200: {
                    items: 3,
                    margin: 20
                }
            }
        });
    }

    // internal operation
    var owlInternalOperation = $('.owl-gallery-office');
    if (owlInternalOperation.length) {
        owlInternalOperation.owlCarousel({
            items: 7,
            rewind: true,
            autoplay: true,
            loop: false,
            lazyLoad: false,
            mouseDrag: true,
            touchDrag: true,
            margin: 10,
            smartSpeed: 250,
            autoplaySpeed: 1000,
            nav: false,
            dots: true,
            responsiveClass:true,
            responsiveRefreshRate: 200,
            responsive: {
                0: {
                    items: 3,
                    margin: 0
                },
                400: {
                    items: 3,
                    margin: 10
                },
                600: {
                    items: 3,
                    margin: 10
                },
                768: {
                    items:2,
                    margin: 10
                },
                992: {
                    items: 3,
                    margin: 20
                },
                1200: {
                    items: 3,
                    margin: 20
                }
            }
        });
    }
    // creteria
    var creteria = $('.owl-creteria');
    if (creteria.length) {
        creteria.owlCarousel({
            items: 7,
            rewind: true,
            autoplay: true,
            loop: false,
            lazyLoad: false,
            mouseDrag: true,
            touchDrag: true,
            margin: 10,
            smartSpeed: 250,
            autoplaySpeed: 1000,
            nav: false,
            dots: false,
            responsiveClass:true,
            responsiveRefreshRate: 200,
            responsive: {
                0: {
                    items: 1,
                    margin: 0
                },
                400: {
                    items: 1,
                    margin: 10
                },
                600: {
                    items: 2,
                    margin: 10
                },
                768: {
                    items:2,
                    margin: 10
                },
                992: {
                    items: 3,
                    margin: 20
                },
                1200: {
                    items: 3,
                    margin: 10
                }
            }
        });
        // $('.prev-traditionalOutstanding').click(function() {
        //     $('.owl-traditionalOutstanding').trigger('prev.owl.carousel');
        // });
        // $('.next-traditionalOutstanding').click(function() {
        //     $('.owl-traditionalOutstanding').trigger('next.owl.carousel');
        // });
    }
    // zoom gallery

    $('.slider').slick({
        slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            asNavFor: '.slider-nav'
    });
    $('.slider-nav').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.slider',
        dots: true,
        arrows: true,
        centerMode: false,
        focusOnSelect: true
    });

});


