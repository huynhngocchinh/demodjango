from .helpers import connect_api


def get_api_list_filter_direction():
    url = 'https://azoffice.vn/api/v3/azoffice/getListDirection'
    method = "post"
    return connect_api(url, method)


def get_building_by_direction(data='', page=0):
    url = f'https://azoffice.vn/api/v3/azoffice/getBuildingByDirection?page={page}'
    method = "post"
    return connect_api(url, method)