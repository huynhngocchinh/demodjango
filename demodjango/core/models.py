from django.db import models

# Create your models here.

# class Member(models.Model):
#   firstname = models.CharField(max_length=255)
#   lastname = models.CharField(max_length=255)


class Office(models.Model):
  name_building = models.CharField(max_length=255)
  url_building = models.CharField(max_length=255)
  district = models.CharField(max_length=30)
  address = models.CharField(max_length=255)
  square = models.CharField(max_length=50)
  image = models.CharField(max_length=255)
  price = models.CharField(max_length=30)
  intro = models.TextField()
  des = models.TextField()
  location_building = models.TextField()
  vat = models.CharField(max_length=50)
  manage_fee = models.CharField(max_length=100)
  car_fee = models.CharField(max_length=50)
  moto_fee = models.CharField(max_length=100)
  overtime_fee = models.CharField(max_length=100)
  electric_fee = models.CharField(max_length=100)
  price_old = models.DecimalField(max_digits=8, decimal_places=4)
  price_new = models.DecimalField(max_digits=8, decimal_places=4)
  iframe = models.TextField()
  so_tang = models.CharField(max_length=30)
  dien_tich_san = models.CharField(max_length=30)
  tong_dien_tich_su_dung = models.CharField(max_length=30)
  so_tang_ham = models.CharField(max_length=30)
  thang_may = models.CharField(max_length=30)
  do_cao_tran = models.CharField(max_length=30)
  created_at = models.DateTimeField()

  class Meta:
    db_table = 'office'  # Set the custom table name
