from .helpers import connect_api


def get_api_list_filter_price():
    url = 'https://azoffice.vn/api/v3/azoffice/getListPrice'
    method = "post"
    return connect_api(url, method)


def get_api_filter_building_feature_by_price(data, page=0):
    url = 'https://azoffice.vn/api/v3/azoffice/getBuildingByPrice?page={page}'
    method = "post"
    return connect_api(url, method, data)

