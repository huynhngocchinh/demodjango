from .helpers import connect_api


def get_Api_contact(data=''):
    url = 'https://azoffice.vn/api/v9/azoffice/getInfoContact'
    connect_api(url)


def get_api_ground(data=''):
    url = 'https://azoffice.vn/api/v10/azoffice/postInfoSpaceBuilding'
    connect_api(url)


def get_api_appointment(data=''):
    url = 'https://azoffice.vn/api/v5/azoffice/postBuildingSchedule'
    connect_api(url)