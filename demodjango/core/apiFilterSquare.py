from .helpers import connect_api


def get_api_list_filter_square():
    url = 'https://azoffice.vn/api/v3/azoffice/getListSquare'
    method = "post"
    return connect_api(url, method)


def get_api_filter_building_feature_by_square(data,page=0):
    url = f'https://azoffice.vn/api/v3/azoffice/getBuildingBySquare?page={page}'
    method = "post"
    return connect_api(url, method)

