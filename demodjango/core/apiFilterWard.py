from .helpers import connect_api


def get_api_list_filter_ward(idDistrict):
    url = 'https://azoffice.vn/api/v3/azoffice/getListWard'
    method = "post"
    return connect_api(url, method, idDistrict)


def get_api_filter_building_feature_by_ward(data):
    url = f'https://azoffice.vn/api/v3/azoffice/getBuildingByWard?page={page}'
    method = "post"
    key = ("urlBuilding", "districtBuilding")
    return connect_api(url, method, data, key)


