from .helpers import connect_api


def get_province():
    url = 'https://azoffice.vn/api/v7/azoffice/getListProvince'
    method = "post"
    return connect_api(url, method)

