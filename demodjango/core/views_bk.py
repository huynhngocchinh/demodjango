from django.http import HttpResponse
from django.template import loader
from .apiBuilding import get_count_office_by_district, get_count_office_by_district_in_header, get_api_list_building_feature, get_list_traditional_feature_building, get_api_detail_building
from .apiBlog import get_lasted_list_blog, get_blog_building_page
from .apiFilterDirection import get_building_by_direction, get_api_list_filter_direction
from .apiFilterDistrict import get_list_district_in_province, get_api_list_filter_district, get_api_district_by_id, get_api_building_in_the_same_district
from .apiFilterPrice import get_api_list_filter_price, get_api_filter_building_feature_by_price
from .apiFilterProvince import get_province
from .apiFilterRoad import get_api_list_filter_road, get_api_filter_building_feature_by_road, get_api_building_in_the_same_road
from .apiFilterSquare import get_api_list_filter_square, get_api_filter_building_feature_by_square
from .apiFilterStandard import get_api_list_filter_standard, get_api_filter_building_feature_by_standard
from .apiFilterWard import get_api_list_filter_ward, get_api_filter_building_feature_by_ward
from django.core.cache import cache
import requests
from .models import Office

def blog(request):
    list_blog_building_page = get_blog_building_page()
    template = loader.get_template('myfirst.html')
    context = {
        'list_blog_building_page': list_blog_building_page
    }
    return HttpResponse(template.render(context, request))


def homepage(request):
    # get data for the page
    office = Office.objects.all()
    # print(office[0].name_building, office[0].price)
    # list_outstanding_traditional_building = get_count_office_by_district('van-phong-truyen-thong')
    # list_outstanding_sharing_building = get_count_office_by_district('van-phong-chia-se')
    # list_lasted_blogs = get_lasted_list_blog()

    # if cache.get('list_district_in_province_header') is not None:
    #     list_district_in_province_header = cache.get('list_district_in_province_header')
    # else:
    #     cache.set('list_district_in_province_header', get_count_office_by_district_in_header())
    #     list_district_in_province_header = cache.get('list_district_in_province_header')

    # set template
    template = loader.get_template('homepage.html')
    context = {
        # 'list_outstanding_traditional_building': list_outstanding_traditional_building,
        # 'list_outstanding_sharing_building': list_outstanding_sharing_building,
        # 'list_district_in_province_header': list_district_in_province_header,
        # 'list_lasted_blogs': list_lasted_blogs
    }
    # return list_outstanding_traditional_building
    return HttpResponse(template.render(context, request))


def traditional_building(request):
    page : get from parameter
    page = 0
    building_type = "van-phong-truyen-thong"
    list_traditional_office = get_list_traditional_feature_building(building_type, page)
    list_district_in_province = get_count_office_by_district(building_type)
    list_price = get_api_list_filter_price()
    list_square = get_api_list_filter_square()
    list_direction = get_api_list_filter_direction()
    list_standard = get_api_list_filter_standard()
    template = loader.get_template('building_list.html')
    id_type = "van-phong-truyen-thong"

    context = {
        'list_traditional_office': list_traditional_office,
        'list_district_in_province': list_district_in_province,
        'list_traditional_offices': list_traditional_office['data'],
        'total_page': list_traditional_office['last_page'],
        'list_price': list_price,
        'list_square': list_square,
        'list_direction': list_direction,
        'list_standard': list_standard,
        'id_type': id_type,
        'page': page
    }

    return HttpResponse(template.render(context, request))


def detail_building(request, slug):

    template = loader.get_template('building_detail.html')
    office = get_api_detail_building(slug)

    context = {
        'office': office
    }
    return HttpResponse(template.render(context, request))


def list_building_filter_in_price(request, type_filter, type_price, id_type, page):
    print(type_filter, type_price, id_type, page)

    template = loader.get_template('building_list.html')
    # office = get_api_detail_building(slug)
    #
    context = {
        # 'office': office
    }
    return HttpResponse(template.render(context, request))


def list_building_in_district(request):
    return 0


def test():
    url = "https://azoffice.vn/api/v7/azoffice/getDistrictByProvince"
    header = {
        "Content-Type": "application/json",
        "Accept": "application/json",
    }
    method = "POST"
    params = {
        'keyEncrypt': 'jfwVrPPbV6ZNKKPpk8R5',
        'typeSlug': data
    }

    result = requests.post(url, json=params, headers=header)
    return 'aaaa'





