from django.http import HttpResponse
from django.template import loader
from .apiBuilding import get_count_office_by_district, get_count_office_by_district_in_header, get_api_list_building_feature, get_list_traditional_feature_building, get_api_detail_building
from .apiBlog import get_lasted_list_blog, get_blog_building_page, get_detail_blog, get_list_posts_activities_azoffice, \
    get_list_posts_generalknowledge, get_list_posts_office_decoration
from .apiFilterDirection import get_building_by_direction, get_api_list_filter_direction
from .apiFilterDistrict import get_list_district_in_province, get_api_list_filter_district, get_api_district_by_id, get_api_building_in_the_same_district
from .apiFilterPrice import get_api_list_filter_price, get_api_filter_building_feature_by_price
from .apiFilterProvince import get_province
from .apiFilterRoad import get_api_list_filter_road, get_api_filter_building_feature_by_road, get_api_building_in_the_same_road
from .apiFilterSquare import get_api_list_filter_square, get_api_filter_building_feature_by_square
from .apiFilterStandard import get_api_list_filter_standard, get_api_filter_building_feature_by_standard
from .apiFilterWard import get_api_list_filter_ward, get_api_filter_building_feature_by_ward
from django.core.cache import cache
import requests
from .models import Office
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

#
# def blog(request):
#     list_blog_building_page = get_blog_building_page()
#     template = loader.get_template('myfirst.html')
#     context = {
#         'list_blog_building_page': list_blog_building_page
#     }
#     return HttpResponse(template.render(context, request))


def blog(request):
    get_lasted_list_blogs = get_lasted_list_blog()
    get_list_posts_activities_azoffices = get_list_posts_activities_azoffice()
    get_list_posts_office_decorations = get_list_posts_office_decoration()
    get_list_posts_generalknowledges = get_list_posts_generalknowledge()

    first_posts_activities_azoffice = get_list_posts_activities_azoffices[0]
    first_posts_office_decoration = get_list_posts_office_decorations[0]
    first_posts_generalknowledge = get_list_posts_generalknowledges[0]

    template = loader.get_template('blog.html')
    context = {
        'get_lasted_list_blog': get_lasted_list_blogs,
        'get_list_posts_activities_azoffice':get_list_posts_activities_azoffices,
        'get_list_posts_office_decoration': get_list_posts_office_decorations,
        'get_list_posts_generalknowledge': get_list_posts_generalknowledges,

        'first_posts_activities_azoffice':first_posts_activities_azoffice,
        'first_posts_office_decoration':first_posts_office_decoration,
        'first_posts_generalknowledge': first_posts_generalknowledge
    }
    return HttpResponse(template.render(context, request))


def blogDetails(request, slug):
    get_detail_blogs = get_detail_blog(slug)
    get_lasted_list_blogs = get_lasted_list_blog()
    get_list_posts_activities_azoffices = get_list_posts_activities_azoffice()

    print(get_detail_blogs)
    template = loader.get_template('detail.html')
    context = {
        'get_lasted_list_blog': get_lasted_list_blogs,
        'get_list_posts_activities_azoffice': get_list_posts_activities_azoffices,
        'get_detail_blogs': get_detail_blogs
    }
    return HttpResponse(template.render(context, request))


def homepage(request):
    list_outstanding_traditional_building = get_count_office_by_district('van-phong-truyen-thong')
    list_outstanding_sharing_building = get_count_office_by_district('van-phong-chia-se')
    list_lasted_blogs = get_lasted_list_blog()
    if cache.get('list_district_in_province_header') is not None:
        list_district_in_province_header = cache.get('list_district_in_province_header')
    else:
        cache.set('list_district_in_province_header', get_count_office_by_district_in_header())
        list_district_in_province_header = cache.get('list_district_in_province_header')

    if cache.get('list_outstanding_traditional_building') is not None:
        list_outstanding_traditional_building = cache.get('list_outstanding_traditional_building')
    else:
        cache.set('list_outstanding_traditional_building', get_count_office_by_district_in_header())
        list_outstanding_traditional_building = cache.get('list_outstanding_traditional_building')

    template = loader.get_template('homepage.html')
    context = {
        'list_outstanding_traditional_building': list_outstanding_traditional_building,
        'list_outstanding_sharing_building': list_outstanding_sharing_building,
        'list_lasted_blogs': list_lasted_blogs
    }
    return HttpResponse(template.render(context, request))


def traditional_building(request):
    page = 0
    building_type = "van-phong-truyen-thong"
    list_traditional_office = get_list_traditional_feature_building(building_type, page)
    list_district_in_province = get_count_office_by_district(building_type)
    list_price = get_api_list_filter_price()
    list_square = get_api_list_filter_square()
    list_direction = get_api_list_filter_direction()
    list_standard = get_api_list_filter_standard()
    template = loader.get_template('building_list.html')
    id_type = "van-phong-truyen-thong"

    if cache.get('list_outstanding_traditional_building') is not None:
        list_outstanding_traditional_building = cache.get('list_outstanding_traditional_building')
    else:
        cache.set('list_outstanding_traditional_building', get_count_office_by_district_in_header())
        list_outstanding_traditional_building = cache.get('list_outstanding_traditional_building')

    context = {
        'list_traditional_office': list_traditional_office,
        'list_district_in_province': list_district_in_province,
        'list_traditional_offices': list_traditional_office['data'],
        'total_page': list_traditional_office['last_page'],
        'list_price': list_price,
        'list_square': list_square,
        'list_direction': list_direction,
        'list_standard': list_standard,
        'id_type': id_type,
        'page': page,
        'list_outstanding_traditional_building': list_outstanding_traditional_building
    }

    return HttpResponse(template.render(context, request))


def detail_building(request, slug):

    template = loader.get_template('building_detail.html')
    office = get_api_detail_building(slug)

    # office in the same street
    data = (office['url_building'], office['street_id'])

    office_in_same_road = get_api_building_in_the_same_road(data)

    # office in the same ward
    data_for_same_district = (office['url_building'], office['district_id'])
    office_in_same_district = get_api_building_in_the_same_district(data_for_same_district)

    if cache.get('list_outstanding_traditional_building') is not None:
        list_outstanding_traditional_building = cache.get('list_outstanding_traditional_building')
    else:
        cache.set('list_outstanding_traditional_building', get_count_office_by_district_in_header())
        list_outstanding_traditional_building = cache.get('list_outstanding_traditional_building')

    context = {
        'office': office,
        'list_outstanding_traditional_building': list_outstanding_traditional_building,
        'office_in_same_road': office_in_same_road,
        'office_in_same_district': office_in_same_district
    }

    return HttpResponse(template.render(context, request))


def list_building_filter_in_price(request, type_filter, type_price, id_type, page):
    print(type_filter, type_price, id_type, page)
    office = get_api_filter_building_feature_by_price(type_filter, type_price)
    template = loader.get_template('building_list.html')
    # office = get_api_detail_building(slug)
    #
    context = {
        # 'office': office
    }
    return HttpResponse(template.render(context, request))


def list_building_in_district(request, slug):
    district_id = slug
    id_type = 1
    data = (id_type, district_id)
    page = 0

    list_offices = get_api_list_filter_district(data, page)
    template = loader.get_template('building_list.html')
    print(list_offices)
    context = {
        'list_traditional_office': list_offices,
    }
    return HttpResponse(template.render(context, request))



def test():
    url = "https://azoffice.vn/api/v7/azoffice/getDistrictByProvince"
    header = {
        "Content-Type": "application/json",
        "Accept": "application/json",
    }
    method = "POST"
    params = {
        'keyEncrypt': 'jfwVrPPbV6ZNKKPpk8R5',
        'typeSlug': data
    }

    result = requests.post(url, json=params, headers=header)
    return 'aaaa'





