# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.

import requests
import json


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press Ctrl+F8 to toggle the breakpoint.


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    url = "https://azoffice.vn/api/v8/azoffice/getPostInBuildingPage"
    header = {
        "Content-Type": "application/json",
        "Accept": "application/json",
    }
    method = "POST"
    params = {
        'keyEncrypt': 'jfwVrPPbV6ZNKKPpk8R5',
        'typeSlug': 'van-phong-truyen-thong'
    }

    result = requests.post(url, json=params, headers=header).json()
    print(result['datas'])
    for item in result['datas']:
        print(item['id'])
        for office in item['list_district']:
            print(office['district_name'])


# See PyCharm help at https://www.jetbrains.com/help/pycharm/
